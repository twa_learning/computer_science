# deadline monotonic scheduling
* https://www.youtube.com/watch?v=D5jlciHNTcw&list=PL9S7pHvQESJFrkuCwVuJwC4aeX_BXpU2Z
* one period is LCM(P1, ..)
* priority: the smallest deadline has the highest priority

# rate monotonic scheduling
* https://www.youtube.com/watch?v=oTGQXqD3CNI&list=PL9S7pHvQESJFrkuCwVuJwC4aeX_BXpU2Z&index=2
* the smaller period has the higher priority

# earliest deadline first scheduling 
* https://www.youtube.com/watch?v=P80NcKUKpuo&list=PL9S7pHvQESJFrkuCwVuJwC4aeX_BXpU2Z&index=3
* priority: the current closest deadline has the highest priority

# least laxity first scheduling / Least slack time scheduling
* https://www.youtube.com/watch?v=0-0ASSc5Gvo&index=4&list=PL9S7pHvQESJFrkuCwVuJwC4aeX_BXpU2Z
* L(T) = D(T) - C(T)
    * D is the current deadline
    * C is the remaining executing times
* the smallest L has the highest priority
