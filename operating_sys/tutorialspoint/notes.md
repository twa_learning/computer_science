# Processes
* https://www.tutorialspoint.com/operating_system/os_processes.htm
* A process is defined as an entity which represents the basic unit of work to be implemented in the system.
* structure inside main memory
    * stack, heap, data, text
        * stack contains the method/function parameters
        * heap contains dynamically allocated memory to process

* process life cycle
    * start, ready, running, waiting, terminated or exit
* process control block
    * data instructure storing process info
    * diff from operating systems

# process scheduling
* https://www.tutorialspoint.com/operating_system/os_process_scheduling.htm
* activity of the process manager
* OS maintains important process scheduling queues
    * job q, ready q, device q
* manage each q using diff policies (FIFO, round robin, priority, etc.)
* long-term, short term, medium term scheduler
* context switches
    * save state and restore state of a process