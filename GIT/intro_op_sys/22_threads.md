* one process -> multiple threads -> can access multiple cpu cores

---

* active entity
    * executing unit of a process
* works simultaneously with others
    * many threads executing
* requires coordination
    * sharing of IO devices, cpu,  memory

# 1 review

![](./fig/22/1.png)

# 3. process vs thread
* share all of the virtual to physical address mappings: code, data, files
* diff regs. stack
* PCB more complex: shared among threads, per-thread exec content

![](./fig/22/3.jpg)

# 4. why are threads useful
* parallelization -> speed up
* specialization -> hot cache
* efficiency -> lower mm requirement & cheaper IPC

---

* when multi processing
    * adress space and execution context -> must be seperately specified
* when multi threaded single process
    * one address space -> multiple execution context
    * passing data among processes -> IPC -> more costly

![](./fig/22/4.jpg)

# 5. Benefits of multithrading: single CPU, when num of threads > num of cpus
* if t_idle > 2 * t_ctx_switch:
     * then context switch to hide idling time
* t_ctx_switch threads < t_ctx_switch processes
* hide latency

![](./fig/22/5.jpg)

# 6. benefits to applications and OS code

![](./fig/22/6.jpg)

# 7.quiz
* threads can share a virtual address space
* processes take longer to context switch
* both t and p have an execution context
* threads usually result in hotter caches when multiple exist
* both make use of some communication mechanisms

# 8. basic thread mechanisms
* thread data structure
     * identify threads, keep track of resource usage ...
* mechanisms to create and manage threads
* mechanisms to safely coordinate among threads running concurrently in the same address space

![](./fig/22/8.jpg)

---

Synchronization mechanisms
* mutual exclusion
     * exclusive access to only one thread at a time
     * mutex
* waiting on other threads
     * specific condition before proceeding
     * condition variable
* waking up other threads from wait state

# 9. thread creation

![](./fig/22/9.jpg)

# 10. thread creation example

![](./fig/22/10.jpg)

# 11. mutexes

![](./fig/22/11.jpg)

# 12. mutexes exclusion
* critical section
     * the portion of the code protected by the mute

![](./fig/22/12.jpg)

![](./fig/22/12_1.jpg)

# 13. mutex example

![](./fig/22/13.jpg)

# 14. quiz

![](./fig/22/14.jpg)

# 15. producer and consumer example

![](./fig/22/15.png)

![](./fig/22/15_1.png)

# 16. condition variables

![](./fig/22/16.png)

* Wait -> enter the waiting list of the mutex

# 17. condition variable API

![](./fig/22/17.png)

# 18. quiz
TODO

![](./fig/22/18.png)

# 19. reader/writer problem

![](./fig/22/19.png)

# 20-23. R/W example 1

![](./fig/22/20.png)

```
In the readers code, there is a typo in the unlocking phase of the critical section:

    if( readers == 0 ) should be if( resource_counter == 0 )
```

![](./fig/22/23.png)
![](./fig/22/23_1.png)

# 24. critical section structure with proxy

![](./fig/22/24.png)

# 25 common pitfalls
* other
    * spurious wake ups
    * deadlocks

# 26. spurious wake ups
* waste cycles by basically context switching these threads to run on the CPU and then back again to wait on the wait queue

# 27-29. deadlocks
* def. 
    * two or more competing threads are waiting on each other to complete, but none of them ever do.

![](./fig/22/28.png)

![](./fig/22/28_1.png)

![](./fig/22/28_2.png)

![](./fig/22/29.png)

# 30. quiz

![](./fig/22/30.png)

# 31. kernel vs user lavel threads
* kernel-level threads are managed by kernel-level scheduler
* user-level threads must be associated with kernel-level threads seperately

# 32. multithreading models

![](./fig/22/32.png)

![](./fig/22/32_1.png)

![](./fig/22/32_2.png)

# 33. scope of multithreading
* process scope
    * more threads in one process -> only half of time on CPU
* system scope
    * more threads in one process -> more resource

![](./fig/22/33.png)

![](./fig/22/33_1.png)
# 35. boss workers pattern

![](./fig/22/35.png)

![](./fig/22/35_1.png)

![](./fig/22/35_2.png)
# 36. how many workers?
* on demand
* pool of workers
* static vs. dynamic

![](./fig/22/36.png)

# 37. boss worker variants

![](./fig/22/37.png)

# 38. pipeline

![](./fig/22/38.png)

![](./fig/22/38_1.png)
# 39. layered pattern

![](./fig/22/39.png)
# 40. quiz
```
Boss-Worker Forumla:

    time_to_finish_1_order * ceiling (num_orders / num_concurrent_threads)

Pipeline Forumla:

    time_to_finish_first_order + (remaining_orders * time_to_finish_last_stage)
```


![](./fig/22/40.png)

