# 1 lesson preview
* kernel vs. user-level threads
* interrupts and signal

# 2. kernel vs user level threads

![](./fig/24/2.png)

# 3. thread data structures: single CPU

![](./fig/24/3.png)

* thread_lib in user-level
    * decide which thread in the process will be scheduled onto the kernel level thread

![](./fig/24/3_1.png)

# 4. thread-related data structures

![](./fig/24/4.png)

# 5. hard and light process state

* PCB contains specific info about just only kernel level thread
    * signals
    * sys call args
* seperate PCB into 
    * hard process state
    * light process state

![](./fig/24/5.png)

# 6. Rationale for multiple datastructures

![](./fig/24/6.png)

# 7 quiz
1. what is the name of the kernel thread structure (name of c structure)
    * kthread_worker
2. what is the name of the data structure - contained in the above structure - that describes the process the kernel thread is running
    * task_struct
* v3.17 of linux kernel

---

* https://elixir.bootlin.com/linux/v3.17/source
* http://www.makelinux.net/kernel_map/

# 8. user level thread data structures

![](./fig/24/8.png)

![](./fig/24/8_1.png)

# 9. kernel level structures in solaris 2.0
![](./fig/24/9.png)

![](./fig/24/9_1.png)

# 10. basic thread management interaction

![](./fig/24/10.png)
* set_concurrency -> request more kernel lavel threads

![](./fig/24/10_1.png)
* the running two threads are waiting for I/O -> blocked
* useful
    * the kernel notify the user -> the user can get more LWPs / KLTs

![](./fig/24/10_2.png)
* when the I/O operations complete
    * cpu send signal to user (because the app uses normally only two threads)

![](./fig/24/10_3.png)

# 11. quiz: Pthread concurrency
* in the pthreads library, which function sets the concurrency level?
    * pthread_setconcurrency()
* for the above function, which concurrency value instructs the implementation to manage the concurrency level as it deems appropriate (integer)?
    * NULL
* http://man7.org/linux/man-pages/man3/pthread_getconcurrency.3.html

# 12. thread management visibility and design

![](./fig/24/12.png)
* the user thread can be bound with a kernel level thread

![](./fig/24/12_1.png)
* one UL and the corresponding KL thread locks a mutex
* but the KL thread is preempted
* the other ULTs can not continue
* only when the KL t with mutex is scheduled -> and the mutex is released -> other ULTs can continue

![](./fig/24/12_2.png)

![](./fig/24/12_3.png)

* ULlib scheduler
    * runs on ULT operations
    * runs on signals from timer or signals

# 13. issues on multiple CPUs
![](./fig/24/13.png)

![](./fig/24/13_1.png)

![](./fig/24/13_2.png)

![](./fig/24/13_3.png)

# 14. synchronization-related issues
![](./fig/24/14.png)
* better spinning on this CPU, when the locked mutex is very short
* this kind of mutex is called "adaptive mutexes"
* single CPU -> no sense to use adaptive mutex

![](./fig/24/14_1.png)

# 15. quiz

![](./fig/24/15.png)

# 16. interrupts and signals intro

![](./fig/24/16.png)

![](./fig/24/16_1.png)

# 17 

* handled in specific ways
    * interrupt and signal handlers
* can be ignored
    * interrupt/signal mask
* expected or unexpected
    * appear sync or async

# 18. interrupt handling

![](./fig/24/18.png)

# 19. signal handling

![](./fig/24/19.png)

![](./fig/24/19_1.png)
* timeout -> async signal

# 20. whydisable interrupts or signals
TODO

# 21 more on signal masks
![](./fig/24/21.png)

# 22. Interrupts on multicore systems
![](./fig/24/22.png)
* allow only one cpu to handle the interrupt -> enhance performance

# 23. types of signals

![](./fig/24/23.png)

# 24. quiz
* http://pubs.opengroup.org/onlinepubs/9699919799/

![](./fig/24/24.png)

# 25. interrupts as threads
TODO

# 26. interrupts: top vs bottom half
TODO 

# 27.

# 28. Threads and signal handling
![](./fig/24/28.png)

# 29. Threads and signal handling: case 1
![](./fig/24/29.png)

# 30. Threads and signal handling: case 2
![](./fig/24/30.png)
* the thread_lib knows which thread has enabled/disabled the signal

# 31. Threads and signal handling: case 3
![](./fig/24/31.png)

# 32. Threads and signal handling: case 4
![](./fig/24/32.png)

TODO

# 33. task struct in linux
* main execution abstraction -> task
    * kernel level thread
* single threaded process -> 1 task
* multi threaded process -> many tasks

![](./fig/24/33.png)

![](./fig/24/33_1.png)

![](./fig/24/33_2.png)
