# process: instance of an executing program
synonymous with task or job

* state of execution
    * program counter, stack
* parts and temporary holding area
    * data, register state occupies state in memory
* may require special hardware 
    * IO devices

# Scheduling
## 3. overview
* CPU scheduler decides how and when processes (and their threads) access shared CPUs
* schedules tasks running user level processes/threads as well as kernel level threads

CPU scheduler:
* chooses one of ready tasks to run on CPU
* runs when
    * cpu becomes idle
    * new task becomes ready
    * timeslice expired timeout

![](./fig/31/3.png)
![](./fig/31/3_1.png)

Context switch, enter user mode, set PC and go <- thread is dispatched on CPU

* scheduling == choose task from ready queue
    * which task should be selected?
        * scheduling policy/ algorithm
    * how is this done?
        * depends on runqueue data structure
    * runqueue <-highly related->scheduling algorithm

## 4. Run to completion scheduling
run task until it finishes

* initial assumptions
    * group of tasks / jobs
    * known execution times
    * no preemption
    * single CPU

* metrics
    * throughput
    * avg job completion time
    * avg job wait time
    * cpu utilization

* FCFS first come first serve
    * schedules tasks in order of arrival
    * in a queue structure -> runqueue
    * ex: t1 = 1, t2 =10, t3 = 1
        * throughput = 3 / 12 = 0.25 tasks/s
        * avg completion time = (1 + 11 + 12) / 3 = 8 sec
        * avg wait time ? (0 + 1 + 11) / 3 = 4 sec

* shortest job first (SJF)
    * schedules tasks in order of their execution time
        * T1 1s > T3 1s > T2 10s
    * runqueue == ordered queue
    * or runqueue == tree
    * assumptions
        * scheduler does not preempt tasks
        * known execution times, t1,t2,t3 like before
        * all arrive at the same time t = 0
    * throughput = 3/12 = 0.25 tasks/s
    * avg comp time = (1 + 2 + 12) / 3 = 5 sec
    * avg wait time = (0 + 1 + 2) / 3 = 1 sec

---

* Throughput Formula:
    * jobs_completed / time_to_complete_all_job
* Avg. Completion Time Formula:
    * sum_of_times_to_complete_each_job / jobs_completed
* Avg. Wait Time Formula:
    * (t1_wait_time + t2_wait_time + t3_wait_time) / jobs_completed
* You do not have to include units in your answers. Also, for decimal answers, please round to the hundredths.
    * time_to_complete_all_job / jobs_completed

---

![](./fig/31/4.png)
![](./fig/31/4_1.png)


## 5. quiz

![](./fig/31/5.png)

## 6. preemptive scheduling: SJF + Preempt
* T1 exec time = 1s, arrival time = 2 sec
* T2 10 sec, 0 sec
* T3 1 sec, 2 sec

---

SJF
* at 2 sec, T2 should be preempted
* heuristics based on history -> estimate the exec time
    * job running time
        * how long did a task run last time?
        * how long did a task run last n times?
        * windowed average

![](./fig/31/6.png)

## 7. priority scheduling

* run highest priority task next (the current task should be preempted)
* the same example as before 
    * priority: T3 > 2 > 1
* multiple runqueues
    * per priority queues
    * tree ordered based on priority
* starvation (potential danger)
    * low priority task waits
    * solution: priority aging -> f(actual prio, time spent in run queue)

![](./fig/31/7.png)
![](./fig/31/7_1.png)
![](./fig/31/7_2.png)
## 8. quiz
* T1 arrival at 5, exec time 3, P1 (highest priority)
* T2 3, 4, P2
* T3 0, 4, P3

---

* This scheduler preempts tasks
* Pay close attention to the ordering of priorities: P3 < P2 < P1

![](./fig/31/8.png)

## 9. priority inversion
?? TODO 

## 10. Round Robin Scheduling
* pick up first task from queue (like FCFS)
* task may yield, to wait on I/O (unlike FCFS)

---

* RR inc. priority
* RR with interleaving -> timeslicing

![](./fig/31/10.png)
![](./fig/31/10_1.png)
![](./fig/31/10_2.png)

## 11. timesharing and timeslices
* timeslice == max. amount of uninterrupted time given to a task -> time quantum
* task may run less than timeslice time 
    * has to wait on I/O, synchronization ... 
        * will be placed on a queue
    * higher priority task becomes runnable
* using timeslices tasks are interleaved -> timesharing the CPU
* CPU bound tasks -> preempted after timeslice

---

timeslice scheduling
* short tasks finish sooner
* more responsive
* lengthy I/O ops initiated sonner
* overheads (-)

---

* TODO how to choose ts

![](./fig/31/11.png)

## 12. how long should a timeslice be
balance benefits and overheads
* I/O bound tasks VS. CPU bound tasks

## 13. CPU bound timeslice length
* 2 tasks, exec. time = 10s
* context switch time = 0.1 s

|alg.|throughput|avg. wait|avg. comp|
|---|---|---|---|
|RR(ts=1)|0.091 tasks/s|0.55s|21.35s|
|RR(ts=5)|0.098 tasks/s|3.05s|17.75s|
|RR ts=inf|0.1|5|15|

* for CPU bound tasks
    * througput and avg.comp are important
    * the user doesn't care about the waiting time
    * bigger ts is better

![](./fig/31/13.png)

## 14. I/O bound tasks
* the same ex as before 
* plus:
    * I/O ops issued every 1s
    * I/O completes in 0.5s

---

* T1 T2 are both I/O bound tasks

|alg.|throughput|avg. wait|avg. comp|
|---|---|---|---|
|RR(ts=1)|0.091 tasks/s|0.55s|21.35s|
|RR(ts=5)|0.091 tasks/s|0.55s|21.35s|

* only T2 is I/O bound task

|alg.|throughput|avg. wait|avg. comp|
|---|---|---|---|
|RR(ts=1)|0.091 tasks/s|0.55s|21.35s|
|RR(ts=5)|0.082 tasks/s|2.55s|17.75s|

* smaller ts is better

![](./fig/31/14.png)

## 15. summary: timeslice
CPU bound prefer longer ts
* limits context switching overheads
* keeps cpu utilization and throughput high

I/O bound prefer shorter ts
* issue I/O ops earlier
* keeps cpu and device utilization high
* better user perceived performance

## 16. quiz ts
```
CPU Utilization Formula:

    [cpu_running_time / (cpu_running_time + context_switching_overheads)] * 100
    The cpu_running_time and context_switching_overheads should be calculated over a consistent, recurring interval

Helpful Steps to Solve:

    Determine a consistent, recurring interval
    In the interval, each task should be given an opportunity to run
    During that interval, how much time is spent computing? This is the cpu_running_time
    During that interval, how much time is spent context switching? This is the context_switching_overheads
    Calculate!
```

On a single CPU system, consider the following workload and conditions
* 10 IO bound tasks and 1 cpu bound task
* I/O bound tasks issue an I/O op every 1ms of cpu computing
* I/O operations always take 10 ms to complete
* context switching overhead is 0.1ms
* all tasks are long running

---

what is the cpu utilization for a round robin scheduler where the timeslice is 1ms? how about for a 10 ms timeslice?

* 1ms / (1ms + 0.1ms) = 0.91 = 91%
* (10 x 1 + 1 x 10) / (10 x 1 + 10 x 0.1 + 1 x 10 + 1 x 0.1) = 95%

![](./fig/31/16.png)

## 17. runqueue data structure
* could be a queue, multiple queues, or a tree
* if we want IO and cpu bound tasks to have different ts values, then ...
    1. same runqueue, check type
    2. two diff structures

---

ex:

* in multi queue data structure
    * ts = 8ms -> most IO intensive
        * highest priority
    * ts = 16 ms -> medium IO intensive -> mix of IO and cpu processing
    * ts = inf (FCFS) -> cpu intensive
        * lowest priority
* how to know IO or cpu intensive
    * history based heuristics
* what about new tasks? and dynamic changing tasks?
    * connect the multi queues
        1. tasks enter topmost queue
        2. if tasks yiels voluntarily -> keep task at this level
        3. if task uses up entire ts -> push down to lower level
        4. task in lower queue gets priority boost when releasing CPU due to I/O waits
    * Multi-Level Feedback Queue (MLFQ) -  Fernando Corbato, turing award winner
        * MLFQ != Priority Queues
        * different treatment of threads at each level

![](./fig/31/17.png)
![](./fig/31/17_1.png)
![](./fig/31/17_2.png)
![](./fig/31/17_3.png)

## 18. Linux O(1) scheduler
* O(1) == constant time to select/add task, regardless of task count
* preemptive, priority-based (0 highest - 140 lowest)
    * priority 0 - 99 -> realtime tasks
    * 100 - 140 other tasks, timesharing
* user processes
    * default 120
    * nice value (-20 to 19)

---

* ts value
    * depends on priority
    * smallest for low priority
    * highest for high priority
* feedback
    * sleep time :  waiting / idling
    * longer sleep -> interactive -> priority - 5 (boost)
    * smaller sleep -> compute intensive -> priority + 5 (lowered)

---

runqueue = 2 arrays of tasks, active and expired
* active
    * used to pick next task to run
    * constant time to add / select
    * tasks remain in queue in active array until timeslice expires
* expired
    * inactive list
    * when no more tasks in active array -> swap active and expired
* highest priority 0 -> ts = 200ms
* lowest priority 140 -> ts= 10ms

---

introduced in linux kernel 2.5 by ingo molnar
* but, workloads changed (gaming, video ...)
    * replaced by CFS in 2.6.23 by Ingo Molnar

![](./fig/31/18.png)
![](./fig/31/18_1.png)
![](./fig/31/18_2.png)

## 19. Linux CFS scheduler
* problems with O(1)
    * inactive list cannot run untill all active tasks are done in ts
    * performance of interactive tasks are affected -> jitter
    * fairness -> time run should be proportianl to their priority

---

* runqueue == Red-black tree
* ordered by v. runtime
* virtual runtime == time spent on CPU

---

* always pick leftmost node -> least amount of CPU runtime
* periodically adjust v. runtime
* compare to leftmost v runtime
    * if smaller, continue running
    * if larger, preempt and place appropriately in the tree
* v runtime progress rate depends on priority and niceness value
    * rate faster for low-priority
        * likely to lose the cpu more quickly
    * rate slower for high priority
* same tree for all priorities

---

* select task -> O(1)
* add task -> o(logN)

![](./fig/31/19.png)
![](./fig/31/19_1.png)
![](./fig/31/19_2.png)

## 20. quiz

![](./fig/31/20.png)

## 21. scheduling on multiprocessors
* shared memory multiprocessor SMP
    * Multiple CPUs have their own caches L1, L2 ...
    * last level cache LLC
    * memory DRAM (shared memory, multiple components)
* multicore
    * CPU with multiple cores -> cores have private L1, L2, ...
    * shared LLC
    * shared system memory

---

* on multiprocessor 
    * diff LLC -> possible schedul on the same CPU before
    * cache-affinity (the cache is hot) -> important
    * hiearchical scheduler architecture
        * per CPU runqueue and scheduler
            * load balance across cpus
                * based on queue length
                * or when CPU is idle
    * non uniform memory access (NUMA)
        * multiple memory nodes
        * memory node closer to a socket of multiple processors
            * access to local mm node faster than access to remote mm node
            * keep tasks on CPU closer to mm node where there state is 
            * NUMA-aware scheduling

![](./fig/31/21.png)
![](./fig/31/21_1.png)
![](./fig/31/21_2.png)
![](./fig/31/21_3.png)

## 22. Hyperthreading
* multiple hardware- supported execution contexts
* still 1 cpu but
* with very fast context switch

---

* hardware multithreading
* hpyerthreading
* chip multithreading CMT
* simultaneous multithreading SMT

---

```
if (t_idle > 2 * t_ctx_switch)
    then context switch to hide latency
```

* SMT ctx_switch - O(cycles)
* memory load - O(100 cycles)

---

* hyperthreading can hide memory access latency

---

* what kinds of threads should we co-schedule on hardware threads?
    * "chip multithreaded processors need a new OS scheduler" by Fedorova et al.

![](./fig/31/22.png)

## 23. Threads and SMT
Assumptions
* thread issues instruction on each cycle
    * max instruction -per-cycle: IPC = 1
* memory access = 4 cycles
* hardware switching instantaneous
* SMT with 2 hardware threads

---

TODO -> understand

## CPU bound or memory bound
TODO



