# 1
* https://s3.amazonaws.com/content.udacity-data.com/courses/ud923/references/ud923-pai-paper.pdf

# 2. which treading model is better?

![](./fig/25/2.png)

# 3. are threads useful?
* Threads are useful because of
    * parallelization -> speed up
    * specialization -> hot cache
    * efficiency -> lower memory requirement and cheaper sync
* threads hide latency of I/O operations (single CPU)

---

* for a matrix multiply application
    * execution time
* for a web service app
    * number of client requests/time
    * response time
    * above -> avg. max, min, 95%
* for hardware
    * higher utilization eg CPU
* depends on metrics

# 4. visual metaphor
* throughput
    * process completion rate
* response time
    * avg. time to respond to input eg mouse click
* utilization
    * percentage of CPU
* many more

# 5. performance metrics intro
metrics == a measurement standard
* measurable and/or quantifiable property
* of the system we're interested in
* that can be used to evaluate the system behavior

examples
* execution time
* software implementation of a problem
* its improvement compared to other implementations

# 6. performance metrics
* performance / dollar
* percentage of SLA violations
* avg res usage
* platform efficiency
* wait time
* execution time
* client-perceived performance
* performance / watt
* throughput
* request rate
* aggregate performance
* cpu utilization

# 7. performance metrics summary
Measurable quantity obtain from
* experiments with real software deployment, real machines, real workloads
* "toy" experiments representative of realistic settings
* simulation
* TESTBED

# 8. really ... are threads useful?
* depends on metrics / workload
* diff num of toy orders -> diff implementation of toy shop
* diff type of graph -> diff shortest path algorithm
* diff file patterns -> diff file sys

# 9. multi process vs multi threads

![](./fig/25/9.png)
* compute header

# 10. multi process web server
![](./fig/25/10.png)

# 11. multi threaded web server
![](./fig/25/11.png)

# 12. event driven model
![](./fig/25/12.png)
* handler
    * run to completion
    * if they need to block
        * init blocking operation and pass control to dispatch loop

# 13. concurrency in the event driven model
![](./fig/25/13.png)

# 14. event drivin model: why
![](./fig/25/14.png)

# 15. event driven model how
![](./fig/25/15.png)

* single address space , single flow of control
* smaller memory requirement, no context switching
* no sync

# 16. helper threads and processes
* a blocking request/handler will block the entire process

---

* asynchronous I/O operations
    * Asynchronous system call
        * process/thread makes system call
        * OS obtains all relevant info from stack, and either learns where to return results, or tells caller where to get results later
        * process/thread can continue
* requires support from kernel eg threads, and/or device eg DMA
* fits nicely with event driven model

---

what if Async calls are not available?

![](./fig/25/16.png)

Healper threads/processes
* Plus
    * resolves portability limitations of basic event driven model
    * smaller footprint than regular worker thread
* minus
    * applicability to certain classes of applications
    * event routing on multi CPU systems

# 17. quiz
models and memory requirements

* of the three models mentioned so far, which model likely requires least amount of memory?
    * event-driven model
    * extra mm only required for helper thread for concurrent blocking I/O, not for all concurrent requests

# 18 flash web server
Flash: event driven web server

* an event driven web server AMPED
* with asymmetric helper processes
* helpers used for disk reads
* pipes used for comm. with dispatcher
* helper reads file in memory via mmap
* dispatcher checks via mincore if pages are in mm to decide local handler or helper

---

* possible big savings

---

![](./fig/25/18.png)

# 19 apache web server

![](./fig/25/19.png)

# 20 experimental methodology

What systems are you comparing?
* MP each process single thread
* MT boss worker
* Single process event driven SPED
* Zeus SPED with 2 processes
* Apache v1.3.1, MP
* Compare against flash AMPED model

---
* for all but Apache optimizations available

What workloads will be used?
* CS web server trace Rice univ
* owlnet trace Rice univ
* synthetic workload

---
* realistic request workload
    * distribution of web page accesses over time
* controlled reproducible workload
    * trace based from real web servers

How will you measure performance
* bandwidth == bytes / time
    * total bytes xfered from files / total time
* connection rate == request/time

---

* evaluate both as  a func of file size
    * larger file size -> ammortize per connection cost -> higher bandwidth
    * larger file size -> more work per connection -> lower connection rate

# 21. experimental results

* synthetic load
    * N requests for same file -> best case
* measure bandwidth
    * BW = N + bytes F / time
    * file size 0-200 kb

TODO