* uses intelligently sized contrainers
    * memory pages or segments
* not all memory is needed at once
    * tasks operate on subset of memory
* optimized for performance
    * reduce time to access state in memory -> better performance

# 3 memory management goals
* allocate
* arbitrate

![](./fig/32/3.png)

![](./fig/32/3_1.png)

![](./fig/32/3_2.png)

# 4 mem managment: hardware support

![](./fig/32/3_2.png)

# 5 page tables

![](./fig/32/5.png)

---

* allocation on first touch
    * allocation only when used
* unused pages == reclaimed
    * long time unused pages -> reclaimed

![](./fig/32/5_1.png)
TODO
---

* OS creates a page table for every process
![](./fig/32/5_2.png)

# 6 page table entry

![](./fig/32/6.png)

![](./fig/32/6_1.png)

![](./fig/32/6_2.png)

