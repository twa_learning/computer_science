* direct operational resources
    * control use of CPU, memory, peripheral devices
* enforce working policies
    * fair resource access, limits to resource usage
* mitigate difficulty of complex tasks
    * abstract hardware details, system calls

# 3. operating system
* hardwares: CPU, main memory, ethernet/wifi, GPU, USB, DISK
* Operating system sit between hardwares and applications
    * directly has privileged access to the underlying hardware
    * hide hardware complexity
        * read/write file storage
        * send/recv socket network
    * resource management
        * scheduling cpu
        * memory manage
    * provide isolation and protection

![](./fig/12/3.png)

# 5. quiz

![](./fig/12/5.png)

# 6. quiz arbitration vs abstraction
* distributing mem between multi processes - arbitr.
* supporting diff types of speakers - abstr
* interchangeable access of hard disk or ssd - abstr 

![](./fig/12/6.png)

# 7. OS examples
* Desktop
    * microsoft windows
    * unix based
        * MAC OS X (BSD)
        * Linux
* embedded
    * android
    * IOS
    * Symbian

# 8. OS Elements
* abstractions
    * process, thread, file, socket, memory page
* mechanisms
    * create, schedule, open, write, allocate
* Policies
    * least-recently used, earliest deadline first

# 9. OS elements example
* Abstractions
    * memory page
* mechanism
    * allocate, map to a process
* policies
    * least recently used-LRU

---

* thread <-> page <-> disk / DRAM

![](./fig/12/9.png)

# 10. principles
* separation of mechanism & policy
    * implement flexible mechanisms to support many policies
    * e.g. LRU, LFU, random
* optimize for common case
    * where used?
    * what be executed
    * what workload requirements

# 11. user/kernel protection boundary
* unprivileged user-level mode
    * applications
* privileged kernel level mode
    * operating system
        * Mm, CPU
    * OS kernel, privileged direct hardware access

---

* user-kernel switch
    * supported by hardware
        * for ex: a privileged bit on CPU
        * trap instructions
            * when the privileged bit is not set, but the user app want to control the hardware
                * the app will be interupted, to verify, to report, or try to terminate the app
    * interface -> system call
        * open, send, malloc
    * signals -> comm among processes

![](./fig/12/11.png)

![](./fig/12/11_1.png)

# 12. system call flowchart

![](./fig/12/12.png)

* to make a sys call an app must
    * write arg
    * save relevant data at well def. location
    * make sys call

![](./fig/12/12_1.png)

* synchronous mode: wait until the sys call completes

# 13. crossing the OS boundary
* user/kernel transitions
    * hardware supported
        * eg traps on illegal instructions or memory accesses requiring special privilege
    * involves a number of instructions
        * e.g ~50-100 ns on a 2GHz machine running linux
    * switches locality
        * affects hardware cache
    * summary -> not cheap

```
Because context switches will swap the data/addresses currently in cache, the performance of applications can benefit or suffer based on how a context switch changes what is in cache at the time they are accessing it.

A cache would be considered hot (fire) if an application is accessing the cache when it contains the data/addresses it needs.

Likewise, a cache would be considered cold (ice) if an application is accessing the cache when it does not contain the data/addresses it needs -- forcing it to retrieve data/addresses from main memory.
```

# 14. OS services
* scheduler -> control the access to the cpu
* mm mgr
* block device driver
* higher level services
    * file system

---

* process, file, device, memo, storage, security mgr
* https://s3.amazonaws.com/content.udacity-data.com/courses/ud923/notes/ud923-p1l2-windows-vs-linux-system-calls.png

![](./fig/12/14.png)
![](./fig/12/14_1.png)

# 15. quiz
64bit linux
* send a signal to a process -> kill
* set the group identity of a process -> setgip
* mount a file sys -> mount
* read/write sys parameters -> sysctl

# 16. monolithic OS
* (+)
    * everything included
    * inlining, copile time optimizations
* (-)
    * customization, portability, manageability
    * memory footprint
    * performance

![](./fig/12/16.png)

# 17. Modular OS
* module(interface) can be added into the op sys dynamically

---

* (+
    * maintainability
    * smaller footprint
    * less resource needs
* (-
    * indirection can impact performance
    * maintenance can still be an issue

![](./fig/12/17.png)

# 18. microkernel
* basic services only
* DB, FS, disk driver are in the user-level

---

* require lots of inter process interactions

---

* (+
    * size
    * verifiability
* (-
    * protability
        * very special to hardwares
    * complexity of software development
        * common components of softwares?
    * cost of user/kernel crossing

![](./fig/12/18.png)

# 19. linux & Mac OS architecture

![](./fig/12/19.png)

![](./fig/12/19_1.png)


