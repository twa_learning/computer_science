* instance of an executing program
* synonymous with task or job

---

* state of execution
    * program counter, stack
* parts and temporary holding area
    * data, register state occupies state in memory
* may require special hardware
    * I/O devices

# 3. process
* OS manages hardwares on behalf of apps
* app == program on disk, flash memory ... (static entity -> at applicapable)
* process == state of a program when executing loaded in memory (active entity) -> diff states

![](./fig/21/3.png)

# 4. process look like
```
        virtual addresses
stack   v_max

...

heap
data
text    v_0
```

* types of state
    * text and data
        * static state when process first loads
    * heap
        * dynamically created during execution
    * stack
        * grows and shrinks - LIFO queue
        * ex: thread state saved -> jump to another thead -> the previous state restored

![](./fig/21/4.png)

# 5. process address space
* address space == in memory representation of a process
* page tables == mapping of virtual to physical addresses

![](./fig/21/5.png)

# 6. address space and memory management
* parts of virtual address space may not be allocated
* may not be enough physical memory for all state

![](./fig/21/6.png)
---

* virtual address 32 bits -> 4 GB
* physical mem -> one process can be parted into diff sections
* some mem may be changed into disk -> save memos

# 7. quiz
* both p1 and p2 can have the exact same range of virtual address space for ex: 0 - 64,000

![](./fig/21/7.png)

# 8. process execution state
* program counter -> count where the line (assembly) is executing
* cpu register -> register the program counter
* stack pointer -> point to the stack LIFO

![](./fig/21/8.png)
---

* Process control block

# 9. process control block
data structure the OS maintains for every one of the process

![](./fig/21/9.png)

* PCB created when the process is created
* certain fields are updated when proces state changes
* other fields change too frequently

# 10. how is a PCB used
* when changing process (context-switch)
    * OS -> save PCB.P1 -> restore PCB.P2 (also update the CPU register == PCB.P2)

![](./fig/21/10.png)

# 11. context switch
* direct costs: n of cycles for load and store instructions
* indirect costs: COLD cache, cache misses
    * processor cache (may have multiple) is much faster than the mm
    * when the data is stored in the cache -> we call that the cache is hot
    * when context switch, the data store in the processor cache is replaced from the mm
        * no data in the cache -> COLD cache
* limit how frequently context switching is done

![](./fig/21/11.png)

# 12. quiz
* when a cache is not
    * most process data is in the cache so the process performance will be at its best
    * sometimes we must context switch

![](./fig/21/12.png)

# 13. process life cycle: states
* read -->scheduler dispatcher--> running
* running --> interrupt --> ready

![](./fig/21/13.png)

* a general illustration of diff states

# 14. quiz
* in states: read and running -> executable

# 15. process life cycle: creates
* ex: https://s3.amazonaws.com/content.udacity-data.com/courses/ud923/notes/ud923-p2l1-process-tree.png
* mechanisms for process creation
    * fork
        * copies the parent PCB int new child PCB
        * child continues execution at instruction after fork
    * exec
        * replace child image
        * load new program and start from the first instruction

![](./fig/21/15.png)

# 16. quiz
* unix-based OS
    * init  -> the parent of all processes
* andoird os
    * ZYGOTE -> parent of all proc

# 17. role of the cpu scheduler
* A  cpu scheduler determines which one of the currently ready processes will be dispatched to the cpu to start running, and how long it should run for

![](./fig/21/17.png)
---

* OS must be efficient:
    * preempt == interrupt and save current context
    * schedule == run scheduler to choose next process
    * dispatch == dispatch process and switch into its context

# 18. length of process
how long should a process run for? how frequently should we run the scheduler

* t_sched == scheduling time
* useful cpu work == total processing time / total time == (2 T_p) / (2 T_p + 2 t_sched)
* if T_p == t_sched -> only 50% of cpu time spent on useful work
    * scheduling work -> overhead
* timeslice == time T_p allocated to a process on the cpu

![](./fig/21/18.png)
---

* scheduling design decisions
    * what are appropriate timeslice values
    * metrics to choose next process to run

# 19. I/O

![](./fig/21/19.png)

# 20. quiz
responsibility of the scheduler
* maintaining the ready queue
* decision on when to context switch 

![](./fig/21/20.png)

# 21. inter process communication IPC
* transfer adta / info between address spaces
* maintain protection and isolation
* provide flexibility and performance

---

* message-passing IPC
    * OS provides communication channel, like shared buffer
    * processes -> write / send , read / recv msg to/from channel
* good : OS manages
* bad : overheads

![](./fig/21/21.png)

* shared memory IPC
    * OS establishes a shared channel and amps it into each process adress space
    * processes directly read/write from this memory
    * os is out of the way
        * no overheads (+)
        * shoud reimplement code (-)

![](./fig/21/21_1.png)
